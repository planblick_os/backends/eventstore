import json
from time import sleep
from db.event import Event
from pbglobal.pblib.pbrsa import Pbrsa
from pbglobal.pblib.pbencryption import PbCrypt
import traceback
import os
import datetime
from pbglobal.pblib.amqp import client


crypto = PbCrypt(os.getenv("EVENT_CRYPTOGRAPHY_KEY"))


def replayEvents(msg):
    event_entries = json.loads(msg).get("rows")

    for entry in event_entries:
        payload = entry.get("payload")
        entry_type = entry.get("type")
        client().publish(toExchange="message_handler", routingKey=entry_type, message=json.dumps(payload))

def event_handler(ch, method=None, properties=None, body=None):
    event = Event()
    event.type = method.routing_key
    try:
        event.payload = crypto.encrypt(body).decode()
    except Exception as e:
        print("---------",body.decode(), "----------------")
        traceback.print_exc()

    payload = body.decode()
    event.correlation_id = json.loads(payload).get("correlation_id")
    event.owner = json.loads(payload).get("owner")
    event.creator = json.loads(payload).get("creator")
    event.kind = json.loads(payload).get("kind", "unknown")
    event.create_time = json.loads(payload).get("create_time", datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))
    event.save()
    ch.basic_ack(method.delivery_tag)

    if method.routing_key == "replayEvents":
        replayEvents(payload)




