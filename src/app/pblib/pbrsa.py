import base64
import os
import rsa
import binascii

def create_keys():
    if os.path.isfile("/src/certs/id_rsa") or os.path.isfile("/src/certs/id_rsa.pem"):
        return
    (pubkey, privkey) = rsa.newkeys(2048, True, 8)

    with open("/src/certs/id_rsa.pem", "w") as text_file:
        text_file.write(pubkey.save_pkcs1().decode('ascii'))
    with open("/src/certs/id_rsa", "w") as text_file:
        text_file.write(privkey.save_pkcs1().decode('ascii'))

def load_priv_key(path="/src/certs/id_rsa"):
    path = os.path.join(os.path.dirname(__file__), path)
    with open(path, mode='rb') as privatefile:
        keydata = privatefile.read()
    return rsa.PrivateKey.load_pkcs1(keydata)


def load_pub_key(path="/src/certs/id_rsa.pem"):
    path = os.path.join(os.path.dirname(__file__), path)
    with open(path, mode='rb') as pubfile:
        keydata = pubfile.read()
    return rsa.PublicKey.load_pkcs1(keydata)


def verify(message, signature, pubkey=None):
    if pubkey is None:
        pubkey = load_pub_key()
    signature = base64.b64decode(signature)
    return rsa.verify(message, signature, pubkey)


def sign(message, privkey=None):
    if privkey is None:
        privkey= load_priv_key()
    signature = rsa.sign(message, privkey, 'SHA-1')
    return base64.b64encode(signature)

def encrypt(string):
    if type(string) == str:
        string = string.encode('utf-8')
    entropy_pwd = base64.b64encode(rsa.encrypt(string, load_pub_key()))
    return entropy_pwd.decode()

def decrypt(crypted):
    if type(crypted) == str:
        crypted = crypted.encode()
    crypted= base64.b64decode(crypted)
    return rsa.decrypt(crypted, load_priv_key()).decode("utf-8")
