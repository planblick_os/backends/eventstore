from db.abstract import Migrate
from sqlalchemy import types
from db.event import Event

def migrate_db():
    print("Creating table Event for Eventstore if not exists")
    Event()
    migration = Migrate()

    # 14.01.2020
    migration.add_column(table_name="events", column_name="create_time", data_type=types.DateTime())

