from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pbglobal.pblib.amqp import client
import datetime

Base = declarative_base()

class Event(DbEntity, Base):
    __tablename__ = 'events'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    owner = Column(String(255))
    creator = Column(String(255))
    correlation_id = Column(String(255))
    kind = Column(String(15), default="command")
    type = Column(String(255))
    payload = Column(Text)
    create_time = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Event(id='%s', type='%s', kind='%s')>\r\n%s" % (self.id, self.type, self.kind, self.payload)

    def save(self):
        self.session.add(self)
        self.session.commit()
        self.session.close()

    def commit(self):
        self.session.commit()

    def add(self):
        self.session.add(self)

    def rollback(self):
        self.session.rollback(self)


