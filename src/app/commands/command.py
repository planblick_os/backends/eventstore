from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pbglobal.pblib.amqp import client
import json

Base = declarative_base()

class Command(DbEntity, Base):
    __tablename__ = 'events'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    owner = Column(String(255))
    creator = Column(String(255))
    correlation_id = Column(String(255))
    kind = Column(String(15), default="command")
    type = Column(String(255))
    payload = Column(Text)

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Command (id='%s', type='%s')>\r\n%s" % (self.id, self.type, self.payload)

    def save(self):
        raise Exception("Commands cannot be saved directly")

    def commit(self):
        raise Exception("Commands cannot be saved directly")

    def add(self):
        raise Exception("Commands cannot be saved directly")

    def rollback(self):
        raise Exception("Commands cannot be saved directly")

    def fire(self):
        amqp_client = client()
        self.payload["correlation_id"] = self.correlation_id
        amqp_client.publish("message_handler", self.type, json.dumps(self.payload))


